import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;


public class Send {

    private final static String FILE_PATH = "E:\\uni\\an4\\sem1\\sd\\activity.txt";
    private final static String QUEUE_NAME = "hello";
    private final static String PATIENT_ID = "c5c1b7c2-b625-4970-8138-c6f532ceb932";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("roedeer-01.rmq.cloudamqp.com");
        factory.setVirtualHost("oxuzxgby");
        factory.setUsername("oxuzxgby");
        factory.setPassword("5xe8hqMsuL51Q3zWUE2ex1W1Y_bfUUiN");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

            BufferedReader reader;

            try {
                reader = new BufferedReader(new FileReader(FILE_PATH));
                String line = reader.readLine();
                while (line != null) {
                    String[] tokens = line.split("(\t\t|\t)");
                    String message = new JSONObject()
                            .put("patient_id", PATIENT_ID)
                            .put("activity", tokens[2])
                            .put("start", tokens[0])
                            .put("end", tokens[1])
                            .toString();

                    channel.basicPublish("", QUEUE_NAME, null, message.getBytes(StandardCharsets.UTF_8));
                    System.out.println(" [x] Sent '" + message + "'");

                    TimeUnit.MILLISECONDS.sleep(500);

                    line = reader.readLine();

                }
                reader.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}